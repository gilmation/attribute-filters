(function($){
  "use strict";
  $.attributeFilters = function(element, options) {
    this.options = {};

    this.init = function(element, options) {
      this.options = $.extend({}, $.attributeFilters.defaultOptions, options);
      if(typeof element.data('attributeFilters') === 'undefined'){
        if(window.Prototype) {
            delete Array.prototype.toJSON;
        }
        this.options.container = $(element).find(this.options.container);
        this.options.item_container = $(element).find('.item');
        this.options.cloned_items = this.options.item_container.clone();
        build_filter_obj(this.options);
        empty_container(this.options);
        build_filter_form(this.options, element);
        this.options.onItemsReady.call(this);

        element.data('attributeFilters', this);
        console.log('done');
      }
    };

    /* Public functions */

    this.init(element, options);
  };

  $.fn.attributeFilters = function(options) {
    return this.each(function() {
      new $.attributeFilters($(this), options);
    });
  };


  /* Private functions */

  var empty_container = function(options){
    options.container.html('');
    return true;
  }

  var set_plugin_state = function(bool, options){
    options.ready = bool;
  }

  var reset_filters = function(filter, value, selected, options, element){
    options.item_container = $(element).find('.item');
    set_selected_filters(filter, value, selected, options);
    set_selected_items(options);
    set_filter_counts(options);
    empty_container(options);
    build_filter_form(options, element);
    set_plugin_state(true, options);
  };

  var set_selected_filters = function(filter, value, selected, options){
    if(selected){
      if(filter in options.selected_filters){
        options.selected_filters[filter].push(value);
      }else{
        options.selected_filters[filter] = [value];
      }

      for(var key in options.selected_filters){
        if(options.selected_filters.hasOwnProperty(key)){
          for(var i = 0 ; i < options.selected_filters[key].length; i++){
            options.filters[key][options.selected_filters[key][i]].selected = true;
          }
        }
     }

    }else{
      options.selected_filters[filter] = jQuery.grep(options.selected_filters[filter], function(v) {
        return v != value;
      });
      if(options.selected_filters[filter].length == 0) delete options.selected_filters[filter];
      options.filters[filter][value].selected = false;
    }
    return true;
  };

  var set_selected_items = function(options){
    var item_result = [];
    for(var filter in options.selected_filters){
      if(options.selected_filters.hasOwnProperty(filter)){
        for(var i = 0; i < options.selected_filters[filter].length; i++){
          var val = options.selected_filters[filter][i];
          if(item_result.length == 0){
            item_result = options.filters[filter][val].items
          }else{
            item_result = intersect_arrays(item_result, options.filters[filter][val].items);
          }
        }
      }
    }
    options.selected_items = item_result;
    return true;
  }

  //Updated the item count for each filter option;
  var set_filter_counts = function(options){
    for(var filter in options.filters){
      if(options.filters.hasOwnProperty(filter)){
        for(var val in options.filters[filter]){
          if(options.filters[filter].hasOwnProperty(val)){
            if(options.selected_items.length > 0){
              options.filters[filter][val].count = intersect_arrays(options.selected_items, options.filters[filter][val].items).length;
            }else{
              options.filters[filter][val].count =  options.filters[filter][val].items.length;
            }
          }
        }
      }
    }
    return true;
  };

  var doCallback = function(options){
    callback.call(this);
  }

  var build_filter_obj = function(options){
    var cache = false,
        uri = window.location.host + window.location.pathname;
    if(typeof(Storage) !== "undefined") {
        //cache = window.sessionStorage.getItem(uri);
    }
    if(cache){
      options.filters = $.parseJSON(cache);
    }else{
      retrieve_range(options);
      //window.sessionStorage.setItem(uri, JSON.stringify(options.filters));
    }
    return true;
  };

  var retrieve_range = function(options){
    $(options.item_container).each(function(){
      for(var key in options.filters){
        if(options.filters.hasOwnProperty(key)){
          var v = transformValue(key, $(this).data(key), options),
              count = 0;
          if(!(v === '')){
            if($(this).is(':visible')){
              count = 1;
            }
            if(!(v in options.filters[key])){
              options.filters[key][v] = {'selected': false, 'count': count};
            }else{
              options.filters[key][v].count = options.filters[key][v].count + count;
            }
            ('items' in options.filters[key][v])? options.filters[key][v].items.push($(this).index()):options.filters[key][v].items = [$(this).index()];
          }
        }
      }
    });
    return true;
  };

  //Generates the filter form
  var build_filter_form = function(options, element){
    for(var key in options.filters){
      var filter = $('<div class="attribute-filters-option"><div class="attribute-filters-option-title" /><div class="attribute-filters-option-items" /></div>');
      filter.find('.attribute-filters-option-title').text(key.charAt(0).toUpperCase() + key.slice(1)); //Capitalize
      var i = 0;
      for(var item in options.filters[key]){
        if(options.filters[key][item].count > 0){
          var label = '';
          // Set Price labels
          if(key === 'price'){
            if(item == 0){
              label = 'Less than '+options.currency+(parseInt(item)+10);
            }else if(item == options.max_price_range){
              label = 'Greater than '+options.currency+item;
            }else{
              label = options.currency+item+' to '+options.currency+(parseInt(item)+10);
            }
          }else{
            label = item;
          }
          var checked = (options.filters[key][item].selected)? ' checked ' : ' ';
          var item_html = $('<a href="#"><input type="checkbox"'+checked+' >'+label+' ('+options.filters[key][item].count+')</a>');

          item_html.data('filter', key);
          item_html.data(key, item);
          item_html.click(function(e){
            if(options.ready){
              options.ready = false;
              var input = $(this).find('input'),
                  key = $(this).data('filter'),
                  val = transformValue(key, $(this).data(key), options);
              if($(e.target).is('a')){ input.prop('checked', !input.prop('checked'));}
              var selected = (input.prop('checked'))?true:false;
              options.filters[key][val].selected = selected;
              reset_filters(key, val, selected, options, element);
              filter_items(options);
              options.onItemsReady.call(this);
            }
            e.preventDefault();
          });
          filter.find('.attribute-filters-option-items').append(item_html);
          i++;
        }
      }
      filter.addClass(key);
      options.container.append(filter);

      if(filter.find('.attribute-filters-option-items a').length > 7){
        filter.append($('<span class="f-small more">More</span>'));

      }
      filter.find('span.more').click(function(){
        var i = $(this).prev();
        i.css('overflow-y','auto');
        i.scrollTop(5);
         $(this).remove();
      });
    }
  };

  var filter_items = function(options){
    var parent = options.item_container.parent();
    if(options.selected_items.length == 0){
      parent.empty().append(options.cloned_items);
    }else{
      parent.empty();
      for(var i = 0; i < options.selected_items.length; i++){
        parent.append(options.cloned_items.eq(options.selected_items[i]));
      }
    }
    return true;
  };

  var transformValue = function(key, value, options){
    if(key === 'price'){value = (value >= options.max_price_range)?options.max_price_range:Math.floor(value /10) * 10;}
    return value;
  };

  var intersect_arrays = function(a, b)
  {
    var ai=0, bi=0;
    var result = new Array();

    while( ai < a.length && bi < b.length )
    {
       if      (a[ai] < b[bi] ){ ai++; }
       else if (a[ai] > b[bi] ){ bi++; }
       else /* they're equal */
       {
         result.push(a[ai]);
         ai++;
         bi++;
       }
    }

    return result;
  }

  /* Default options */
  $.attributeFilters.defaultOptions = {
    container: '',
    item_container: [],
    cloned_items: {},
    filters: {'price': {},
              'brand': {},
              'gender':{}},
    selected_filters: [],
    selected_items:[],
    min_price_range: 20,
    max_price_range: 80,
    currency: '&pound;',
    ready: true,
    onItemsReady: function() {}
  };
})(jQuery);